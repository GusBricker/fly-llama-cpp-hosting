# Fly.io LLama.cpp Hosting

A simple project to host llama.cpp models (GGUF's) on fly.io machines, this is a cheap way to experiment with models. Requires using `fly volumes` for this to work.

## How does this work?

Leverages the health checking endpoint that fly offers to run a periodic check of the fly machine, if the fly machine does not find the model or it doesnt match in size on the volume then it begins downloading the model in the background.
Whilst it is downloading the model, it does not accept any other requests and doesnt start llama.cpp. The fly machine will report unhealthy during this period.

After the model matches size on the fly volume (uses a HEAD request to the `MODEL_URL`), the fly machine starts llama.cpp in a seperate process. If llama.cpp is up and running, then the fly machine reports healthy and begins proxying requests to llama.cpp.

If there are no active requests for `SHUTDOWN_TIMEOUT`, then the fly machine will shut it self down to save costs. Future requests will start the machine back up in which it loads the model from persistent storage on the fly volume much quicker.

## Steps for initial launch:

1. Modify fly.toml, most likely want to change:
- `app`: Name of application, will be directly reflected in url of app (eg my-gpt.fly.dev)
- `primary_region`: Sets the region the app will be deployed in. See https://fly.io/docs/reference/regions/
- `cpu`, `memory`, `cpu_kind`: Adjust to suit the model you are deploying

After fly.tomly is finalised, create the app with `fly launch --wait-timeout 10m -e "API_KEY=<your api key>" -e "MODEL_URL=<your gguf url>"`

Also need to set the following environment variables:
- `MODEL_URL`: model to download and use, download will either begin on the first health check or request to the server
- `API_KEY`: API Key must be set for access to llama.cpp
- `SHUTDOWN_TIMEOUT` (optional, default 300 seconds): Shutdown timeout for the fly machine after inactivity.

Adjust wait-timeout based on the size of your model as the model will download eventually via the health check during the deploy, so for the deploy to be successful then the health check needs to pass.

2. Fly will automatically create a volume gpt_models, this will need to be extended to fit the model you deploy with. Use `fly vol extend -a <app name> -s <size in GB>`

3. Test by visiting `https://<app name>.fly.dev`


## Steps for future deploys
1. Deploy the model again with additional settings: `fly deploy --wait-timeout 10m -e "API_KEY=<your api key>" -e "MODEL_URL=<your gguf url>"`

Also need to set the following environment variables:
- `MODEL_URL`: model to download and use, download will either begin on the first health check or request to the server
- `API_KEY`: API Key must be set for access to llama.cpp
- `SHUTDOWN_TIMEOUT` (optional, default 300 seconds): Shutdown timeout for the fly machine after inactivity.

Adjust wait-timeout based on the size of your model as the model will download eventually via the health check during the deploy, so for the deploy to be successful then the health check needs to pass.

2. Test by visiting `https://<app name>.fly.dev`

#### llama.cpp server hosting documentation:
https://github.com/ggerganov/llama.cpp/tree/master/examples/server