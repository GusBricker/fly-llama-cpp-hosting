import http.server
import socketserver
import requests
import os
import subprocess
import signal
from threading import Lock, Thread, Semaphore
from multiprocessing import Process, Value
from multiprocessing import Lock as MPLock
from urllib.parse import urlparse, parse_qs
import time
from requests.structures import CaseInsensitiveDict
import traceback
import sys

MODEL_URL = os.environ.get('MODEL_URL')
MODEL_PATH = "/models/model.gguf"
API_KEY = os.environ.get('API_KEY')
count = 0
downloading = False
count_lock = Lock()
llamacpp_lock = MPLock()
llamacpp_pid = Value('Q', 0)
last_request = time.time()
shutdown_timeout = int(os.environ.get('SHUTDOWN_TIMEOUT', 300)) # Seconds
ngl_value = str(os.environ.get('N_GPU_LAYERS', 100))
    
def download_model():
    global downloading
    print(f"[Model] Beginning download {MODEL_URL} -> {MODEL_PATH}")
    with requests.get(MODEL_URL, stream=True, allow_redirects=True) as r:
        r.raise_for_status()
        with open(MODEL_PATH, 'wb') as f:
            for chunk in r.iter_content(chunk_size=8192): 
                f.write(chunk)
    downloading = False
    print('[Model] Model downlaod complete')

def have_valid_model():
    if not os.path.exists(MODEL_PATH):
        print('[Model] Model doesnt exist')
        return False

    # Send a HEAD request to the file URL
    response = requests.head(MODEL_URL, allow_redirects=True)

    # Check if the 'Content-Length' header is present in the response
    if 'Content-Length' in response.headers:
        file_size_on_server = int(response.headers['Content-Length'])
        file_size_on_disk = os.path.getsize(MODEL_PATH)
        print(f"[Model] File size on server: {file_size_on_server} bytes")
        print(f"[Model] File size on disk: {file_size_on_disk} bytes")
        if file_size_on_server == file_size_on_disk:
            print('[Model] Model OK')
            return True
        print('[Model] Model not OK')
        return False
    raise Exception("Couldnt determine filesize, no Content-Lenght in headers")

def _start_llamacpp(server_pid, llamacpp_lock):
    llamacpp_args = ['/usr/local/bin/llamacpp-server', '--host', '0.0.0.0', '--port', '8200', '-m', MODEL_PATH, '--api-key', API_KEY, '--embeddings', '--mlock', '-ngl', ngl_value]
    print('[llama.cpp] ', llamacpp_args)
    with llamacpp_lock:
        server_process = subprocess.Popen(
            llamacpp_args,
            stdout=subprocess.PIPE,
            stderr=subprocess.STDOUT,
            encoding='utf-8',
            errors='replace'
        )
    print('[llama.cpp] PID: ', server_process.pid)
    server_pid.value = server_process.pid
    
    try:
        while True:
            realtime_output = server_process.stdout.readline()
            if realtime_output == '' and server_process.poll() is not None:
                print('[llama.cpp] Output done')
                break
            if realtime_output:
                print("[llama.cpp] " + realtime_output.strip(), flush=True)
            else:
                print('[llama.cpp] Output sleeping')
                time.sleep(0.1)
        with llamacpp_lock:
            exit_status = server_process.wait()
        print(f"[llama.cpp] Exit status: {exit_status}")
    except Exception as e:
        print('[llama.cpp] Exception ', e)

    print('[llama.cpp] Monitoring process stopped')
    server_pid.value = 0

def start_llamacpp():
    global llamacpp_pid, llamacpp_lock
    with llamacpp_lock:
        llamacpp_pid.value = 0
    t = Process(target=_start_llamacpp, args=(llamacpp_pid, llamacpp_lock))
    t.start()
    print('[llama.cpp] Started')
    
def stop_llamacpp():
    global llamacpp_pid, llamacpp_lock
    with llamacpp_lock:
        print('[llama.cpp] Shutdown request sent')
        if llamacpp_pid.value:
            print('[llama.cpp] Stopping')
            os.kill(llamacpp_pid.value, signal.SIGTERM)
            while True:
                if not is_llamacpp_running(True, True):
                    break
            llamacpp_pid.value = 0

def is_llamacpp_accepting_requests():
    try:
        r = requests.get('http://127.0.0.1:8200/', timeout=5)
        return r.ok
    except:
        pass
    return False

def is_llamacpp_running(skip_lock=False, ignore_defunct=False):
    global llamacpp_pid, llamacpp_lock
    def _check():
        global llamacpp_pid, llamacpp_lock
        if llamacpp_pid.value:
            try:
                # Execute 'ps' command to get process state
                output = subprocess.check_output(['ps', '-p', str(llamacpp_pid.value), '-o', 'state='])
                # Check if the output contains 'Z' (zombie state) or 'D' (defunct state)
                if b'Z' in output or b'D' in output:
                    print('[llama.cpp] Defunct/Zombied PID: ', llamacpp_pid.value)
                    return False if not ignore_defunct else True
                else:
                    print('[llama.cpp] Running PID: ', llamacpp_pid.value)
                    return True
            except subprocess.CalledProcessError:
                # Process not found, so it's not running or zombied
                print('[llama.cpp] Not Running PID: ', llamacpp_pid.value)
                return False
        else:
            print('[llama.cpp] PID not set')
            return False
    if not skip_lock:
        with llamacpp_lock:
            return _check()
    else:
       return _check()

def check_healthy(wait_healthy=0):
    global downloading, count_lock
    with count_lock:
        if have_valid_model():
            try:
                if not is_llamacpp_running():
                    print('[Executor] Starting llama.cpp server')
                    start_llamacpp()
                    if wait_healthy > 0:
                        start = time.time()
                        while time.time() - start < wait_healthy:
                            if is_llamacpp_running() and is_llamacpp_accepting_requests():
                                return True
                else:
                    print('[Executor] llama.cpp already running')
                    return True
            except Exception as e:
                print('[Executor] Exception checking validity, ', e)
                pass
        else:
            print('[Executor] Model invalid')
            if not downloading:
                downloading = True
                print('[Executor] Beginning model download')
                t = Thread(target=download_model)
                t.start()
    return False

class Proxy(http.server.BaseHTTPRequestHandler):
    timeout = 60

    def do_GET(self):
        global downloading, count_lock
        # Check for the API key in the Authorization header
        print(f'[Proxy] {self.protocol_version} GET rx: {self.path}')
        healthy = check_healthy(10)
        if self.path == '/health':
            if healthy:
                self.send_response(200)
                self.send_header('Content-type', 'text/html')
                self.end_headers()
                self.wfile.write(b'OK')
                with count_lock:
                    diff = time.time() - last_request
                    print(f'[Proxy] Count: {count}')
                    print(f'[Proxy] Time: {diff} vs {shutdown_timeout}')
                    if count == 0 and diff > shutdown_timeout:
                        print('[Proxy] Stopping proxy')
                        stop_llamacpp()
                        sys.exit(0)
            else:
                self.send_response(503) # Service Unavailable until model is ready
                self.end_headers()
                self.wfile.write(b'Service Unavailable')
            return
        self.forward_request()

    def do_POST(self):
        print(f'[Proxy] {self.protocol_version} POST rx: {self.path}')
        self.forward_request()
        
    def do_OPTIONS(self):
        print(f'[Proxy] {self.protocol_version} OPTIONS rx: {self.path}')
        self.forward_request()
        
    def do_HEAD(self):
        print(f'[Proxy] {self.protocol_version} HEAD rx: {self.path}')
        self.forward_request()
        
    def forward_request(self):
        global count, last_request
        with count_lock:
            last_request = time.time()
            count += 1
        try:
            check_healthy(10)
            url = f"http://127.0.0.1:8200{self.path}"
            print('[Proxy] User Headers')
            print(self.headers)
            headers = CaseInsensitiveDict()
            for key, value in CaseInsensitiveDict(self.headers).lower_items():
                if key.startswith('sec-') or key.startswith('fly-') or key.startswith('x-'):
                    continue
                if key == 'transfer-encoding' and value == 'chunked':
                    continue
                if key == 'keep-alive' and value == 'timeout=5, max=5':
                    continue
                headers[key] = value
            if 'origin' in headers:
                headers['origin'] = 'http://127.0.0.1:8000'
            if 'host' in headers:
                headers['host'] = '127.0.0.1:8000'
                
            for key, value in CaseInsensitiveDict(headers).lower_items():
                print(key + ': ' + value)
            
            query_params = parse_qs(urlparse(self.path).query)
            # Determine if the request has a body
            content_length = headers.pop('content-length', None)
            data = None
            if content_length and self.command in ['POST', 'PUT', 'PATCH']:
                data = self.rfile.read(int(content_length))
            
            print(f'[Proxy] Forwarding request to llama.cpp [{self.command}] {url}')

            print('[Proxy] ----')
            response = requests.request(
                self.command,
                url,
                headers=headers,
                data=data,
                params=query_params,
                timeout=1200
            )
            self.send_response(response.status_code)
            print('[Proxy] Response Headers')
            for key, value in response.headers.items():
                if key == 'Transfer-Encoding' and value == 'chunked':
                    continue
                if key == 'Keep-Alive' and value == 'timeout=5, max=5':
                    continue
                self.send_header(key, value)
                print(key + ': ' + str(value))
            self.end_headers()
            self.wfile.write(response.content)
            print(response.content)
            print('[Proxy] ----')
        except Exception:
            traceback.print_exc()
            
        with count_lock:
            last_request = time.time()
            if count > 0:
                count -= 1

if __name__ == "__main__":

    if not API_KEY:
        raise Exception('API_KEY environment variable not set')
    if not MODEL_URL:
        raise Exception('MODEL_URL environment variable not set')
    check_healthy(10)
    PORT = 8000
    Handler = Proxy
    httpd = socketserver.TCPServer(("", PORT), Handler)
    print(f"[Executor] Proxy server running on port {PORT}")
    try:
        httpd.serve_forever()
    except KeyboardInterrupt:
        pass
    finally:
        # Clean-up server (close socket, etc.)
        httpd.server_close()
    print('[Executor] Proxy stopped')
