FROM python:3.9-slim-bookworm as build

RUN apt-get update && \
    apt-get install -y build-essential git cmake wget software-properties-common

WORKDIR /

# Clone ggml repository
RUN git clone -b b2398 https://github.com/ggerganov/llama.cpp

RUN wget https://developer.download.nvidia.com/compute/cuda/repos/debian12/x86_64/cuda-keyring_1.1-1_all.deb && \
    dpkg -i cuda-keyring_1.1-1_all.deb && \
    add-apt-repository contrib && \
    apt update && \
    apt -y install cuda-toolkit-12-4

ENV CUDA_VERSION="12.4"
ENV CUDA_HOME="/usr/local/cuda-${CUDA_VERSION}"
ENV PATH="${CUDA_HOME}/bin${PATH:+:${PATH}}"
RUN nvcc --help
# Build ggml and examples
RUN mkdir /llama.cpp/build
WORKDIR /llama.cpp/build
RUN cmake .. -DCMAKE_CXX_FLAGS="-march=x86-64" -DLLAMA_NATIVE=OFF -DLLAMA_AVX512=OFF -DLLAMA_AVX2=OFF -DLLAMA_AVX=OFF -DLLAMA_FMA=OFF -DLLAMA_F16C=OFF -DLLAMA_BUILD_SERVER=1 -DLLAMA_CUBLAS=on -DCMAKE_CUDA_ARCHITECTURES=all-major
RUN cmake --build . --config Release

FROM python:3.9-slim-bookworm

RUN apt-get update && \
    apt-get install -y python3-pip libmagic-dev procps

RUN pip3 install requests python-magic
COPY --from=build /usr/local/cuda /usr/local/cuda
RUN ldconfig /usr/local/cuda/lib64
COPY --from=build /llama.cpp /llama.cpp
COPY --from=build /llama.cpp/build/bin/server /usr/local/bin/llamacpp-server
COPY entrypoint.py /usr/local/bin/entrypoint.py
ENTRYPOINT ["python3", "/usr/local/bin/entrypoint.py"]

# Expose the specified port
EXPOSE 8000